package com.pulse.simplechatfb;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

        private static String TAG="CHAT activity";




        public static DatabaseReference mSimpleFirechatDatabaseReference;
        private FirebaseRecyclerAdapter<ChatMessage, FirechatMsgViewHolder>
                mFirebaseAdapter;
        private RecyclerView mMessageRecyclerView;
        private LinearLayoutManager mLinearLayoutManager;
        private ProgressBar mProgressBar;
        private Button mSendButton;

    private FirebaseUser firebaseUser;
    public static EditText mMsgEditText;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

        public static class FirechatMsgViewHolder extends RecyclerView.ViewHolder {
            public TextView msgTextView;
            public TextView userTextView;
            public CircleImageView userImageView;


            public FirechatMsgViewHolder(View v) {
                super(v);
                msgTextView = (TextView) itemView.findViewById(R.id.msgTextView);
                userTextView = (TextView) itemView.findViewById(R.id.userTextView);
                userImageView = (CircleImageView) itemView.findViewById(R.id.userImageView);
            }
        }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            mAuth=FirebaseAuth.getInstance();
            startService(new Intent(this, GPSService.class));

            mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
            mMessageRecyclerView = (RecyclerView) findViewById(R.id.messageRecyclerView);
            mLinearLayoutManager = new LinearLayoutManager(this);
            mLinearLayoutManager.setStackFromEnd(true);
            mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
            mMsgEditText =(EditText) findViewById(R.id.msgEditText) ;


            if (mAuthListener==null) {
                mAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (user == null) {
                            Intent intent = new Intent( MainActivity.this,AuthActivity.class);
                            startActivity(intent);

                        }
                    }


                };
            }

            findViewById(R.id.signout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAuth.signOut();
                    onStop();
                }
            });

            mSendButton = (Button) findViewById(R.id.sendButton);
            mSendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(mMsgEditText.getText().toString().trim().length()!=0) {
                        ChatMessage friendlyMessage = new ChatMessage(mMsgEditText.getText().toString().trim(),
                                firebaseUser.getDisplayName()==null?firebaseUser.getEmail():firebaseUser.getDisplayName(),
                                firebaseUser.getPhotoUrl()==null? null :firebaseUser.getPhotoUrl().toString());
                        mSimpleFirechatDatabaseReference.child("messages")
                                .push().setValue(friendlyMessage);
                        mMsgEditText.setText("");
                    }
                }
            });

            mSimpleFirechatDatabaseReference = FirebaseDatabase.getInstance().getReference();


            mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatMessage,
                                FirechatMsgViewHolder>(
                    ChatMessage.class,
                    R.layout.chat_message,
                    FirechatMsgViewHolder.class,
                    mSimpleFirechatDatabaseReference.child("messages")) {

                @Override
                protected void populateViewHolder(FirechatMsgViewHolder viewHolder, ChatMessage friendlyMessage, int position) {
                    mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                    viewHolder.msgTextView.setText(friendlyMessage.getText());
                    viewHolder.userTextView.setText(friendlyMessage.getName());
                    if (friendlyMessage.getPhotoUrl() == null) {
                        viewHolder.userImageView
                                .setImageDrawable(ContextCompat
                                        .getDrawable(MainActivity.this,
                                                R.drawable.avatar));
                    } else {
                        Glide.with(MainActivity.this)
                                .load(friendlyMessage.getPhotoUrl())
                                .into(viewHolder.userImageView);
                    }
                }
            };

            mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);
                    int chatMessageCount = mFirebaseAdapter.getItemCount();
                    int lastVisiblePosition =
                            mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                    if (lastVisiblePosition == -1 ||
                            (positionStart >= (chatMessageCount - 1) &&
                                    lastVisiblePosition == (positionStart - 1))) {
                        mMessageRecyclerView.scrollToPosition(positionStart);
                    }
                }
            });

            mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
            mMessageRecyclerView.setAdapter(mFirebaseAdapter);
            firebaseUser = mAuth.getCurrentUser();
        }



    }